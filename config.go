package main

import (
	"io/ioutil"

	"gopkg.in/yaml.v2"
)

type config struct {
	// the path to the config file
	path string

	// search dir include
	Include []string `yaml:"include"`

	// search dir exclude
	Exclude []string `yaml:"exclude"`

	// execute checks
	Checks []string `yaml:"checks"`

	// specific settings for specific paths
	Paths []struct {
		Checks []string `yaml:"checks"`
	} `yaml:"paths"`

	// indicate that config has been modified and needs to be written to disk
	dirty bool
}

func (c *config) needsSave() bool {
	return c.dirty
}

func (c *config) save() {
	if !c.needsSave() {
		return
	}
	d, err := yaml.Marshal(c)
	if err != nil {
		panic(err)
	}
	err = ioutil.WriteFile(c.path, d, 0644)
	if err != nil {
		panic(err)
	}
}

func NewConfig(path string) *config {
	conf := &config{}
	d, err := ioutil.ReadFile(path)
	if err != nil {
		panic(err)
	}
	err = yaml.Unmarshal(d, conf)
	if err != nil {
		panic(err)
	}
	conf.path = path
	conf.dirty = false
	return conf
}
