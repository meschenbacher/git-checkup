package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/plumbing"
)

type warning struct {
	path string
	repo *git.Repository
	// worktree *git.Worktree
	warnings []string
}

// type myRepository struct{}

type myStatus struct {
	path string
}

// func (r *myRepository) Status() myStatus {
// return myStatus{}
// }

// func (s *myStatus) Status() string {
// return ""
// }

func (s *myStatus) String() string {
	out, err := exec.Command("git", "-C", s.path, "status", "--porcelain").Output()
	if err != nil {
		log.Fatal(err)
	}
	return string(out)
}

func (s *myStatus) Stashes() string {
	out, err := exec.Command("git", "-C", s.path, "stash", "list").Output()
	if err != nil {
		log.Fatal(err)
	}
	return string(out)
}

func (s *myStatus) IsClean() bool {
	return s.String() == ""
}

func (w *warning) appendWarning(msg string) {
	w.warnings = append(w.warnings, msg)
}

func checkStatus(thiswarn *warning, status myStatus, flag_status *bool) {
	branch, err := thiswarn.repo.Head()
	if err != nil {
		// err.Error() is probably "reference not found"
		return
	}
	branchname := branch.Name().String()
	refPrefix := "refs/heads/"
	if strings.HasPrefix(branchname, refPrefix) {
		branchname = branchname[len(refPrefix):]
	}
	msg := fmt.Sprintf("worktree not clean (branch %s)", branchname)
	if *flag_status {
		msg += "\n" + status.String()
	}
	thiswarn.appendWarning(msg)
}

func main() {
	var repos []string
	var exitcode = 0
	flag_interactive := flag.Bool("i", false, "interactively ask to enter each repo")
	flag_status := flag.Bool("status", false, "print status")
	env_config, _ := os.LookupEnv("GIT_CHECKUP_CONF")
	flag_config := flag.String("config", env_config, "specify config file (default GIT_CHECKUP_CONF environment variable)")
	flag.Parse()
	args := flag.Args()
	var conf *config
	if *flag_config != "" {
		conf = NewConfig(*flag_config)
		conf.dirty = true
		args = append(args, conf.Include...)
	}
	repos = make([]string, 0)
	for _, dir := range args {
		err := filepath.Walk(dir, func(path string, info os.FileInfo, err error) error {
			if err != nil {
				exitcode |= 1
				if os.IsPermission(err) {
					fmt.Fprintf(os.Stderr, "Error %v\n", err)
					return filepath.SkipDir
				} else {
					return err
				}
			}
			if !info.IsDir() || filepath.Base(path) != ".git" {
				return nil
			}
			repos = append(repos, filepath.Dir(path))
			return filepath.SkipDir
		})
		if err != nil {
			fmt.Fprintf(os.Stderr, "error walking %s: %v\n", dir, err)
			os.Exit(1)
		}
	}
	var warnings = make([]*warning, 0)
	for _, path := range repos {
		thiswarn := &warning{path: path}
		r, err := git.PlainOpen(path)
		if err != nil {
			fmt.Fprintf(os.Stderr, "open git %s: %s\n", path, err)
			continue
		}
		thiswarn.repo = r
		remotes, err := r.Remotes()
		if err != nil {
			log.Fatal(err)
		}
		if len(remotes) == 0 {
			thiswarn.appendWarning("no remotes")
		}

		status := myStatus{path: path}
		if !status.IsClean() {
			checkStatus(thiswarn, status, flag_status)
		}

		branches, err := r.Branches()
		if err != nil {
			log.Fatal(err)
		}
		branches.ForEach(func(b *plumbing.Reference) error {
			refPrefix := "refs/heads/"
			refName := b.Name().String()
			if !strings.HasPrefix(refName, refPrefix) {
				return nil
			}
			branchName := refName[len(refPrefix):]
			branch, err := r.Branch(branchName)
			if err != nil {
				thiswarn.appendWarning(fmt.Sprintf("Branch %s has no remote", branchName))
				return nil
			}
			if branch.Remote == "" {
				thiswarn.appendWarning(fmt.Sprintf("Branch %s has no remote", branchName))
			}
			return nil
		})

		stashes := status.Stashes()
		if stashes != "" {
			thiswarn.appendWarning("stashes present")
		}

		if len(thiswarn.warnings) > 0 {
			warnings = append(warnings, thiswarn)
		}
	}
OUTER:
	for i, warning := range warnings {
		path := warning.path
		fmt.Printf("%d/%d %s\n", i+1, len(warnings), path)
		for _, warnmsg := range warning.warnings {
			fmt.Printf("\t%s\n", warnmsg)
		}
		if *flag_interactive {
			fmt.Fprintf(os.Stderr, "Start shell to inspect %s? [Y/n/q] ", path)
			for {
				reader := bufio.NewReader(os.Stdin)
				answer, _ := reader.ReadString('\n')
				answer = strings.Replace(answer, "\n", "", -1)
				if answer == "" || answer == "Y" {
					proc, err := os.StartProcess(os.Getenv("SHELL"), []string{os.Getenv("SHELL")}, &os.ProcAttr{Dir: path, Files: []*os.File{os.Stdin, os.Stdout, os.Stderr}})
					if err != nil {
						panic(err)
					}
					_, err = proc.Wait()
					if err != nil {
						panic(err)
					}
					continue OUTER
				} else if answer == "q" {
					os.Exit(exitcode)
				} else if answer == "n" {
					continue OUTER
				} else {
					continue
				}
			}
		}
	}
	os.Exit(exitcode)
}
