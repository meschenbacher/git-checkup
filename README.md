# Obtain a rough overview of the state of local git repos
Implemented checks
- Warn on stashes
- Warn on missing remotes
- Warn on unclean worktree
- Warn on branches without remote

# TODO
- Make a few options configurable via config file
  - directory includes and excludes
  - silence warnings for repos
  - silence warnings (interactively) for tracked upstream branches
- git fetch --all
- check for submodules
